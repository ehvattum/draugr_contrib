#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;

namespace draugr_contrib
{
    public class ResponseHandler : IResponseHandler
    {
        private readonly IMiscDispatcher miscDispatcher;
        private readonly IPwner pwner;
        private GameState state;


        public ResponseHandler(IPwner pwner, IMiscDispatcher miscDispatcher)
        {
            this.pwner = pwner;
            this.miscDispatcher = miscDispatcher;
        }


        public void Handle(GameState gameState)
        {
            this.state = gameState;

            switch (this.state.Message)
            {
                case "connect":
                    this.miscDispatcher.OnHandShakeSuccess();
                    break;
                case "gamestate":
                    this.pwner.Pwn(gameState);
                    break;
                case "action":
                    this.miscDispatcher.PrintAction(gameState);
                    break;
                case "endturn":
                    this.miscDispatcher.OnEndturn();
                    break;
                default:
                    Console.WriteLine("Got unknown message: " + this.state.Message);
                    break;
            }
        }
    }
}