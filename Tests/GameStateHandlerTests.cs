﻿#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using NSubstitute;

using NUnit.Framework;

using Newtonsoft.Json;

using draugr_contrib;

namespace Tests
{
    [TestFixture]
    public class ResponseHandlerTests
    {
        #region Setup/Teardown

        [SetUp]
        public void SetUp()
        {
            this.handler = new ResponseHandler(this.pwner, this.dispatcher);
        }

        #endregion

        private ResponseHandler handler;
        private readonly IPwner pwner = Substitute.For<IPwner>();
        private readonly IMiscDispatcher dispatcher = Substitute.For<IMiscDispatcher>();


        [Test]
        public void BeforeLoadOut_Pwns()
        {
            const string fromServer = @"{'message':'gamestate',
  'turn': 0,
  'map': {'j-length': 5,
          'k-length': 5,
          'data': [
                  ['R', 'G', 'V', 'G', 'V'],
                  ['G', 'E', 'G', 'G', 'S'],
              ['G', 'G', 'O', 'G', 'G'],
              ['S', 'G', 'G', 'C', 'G'],
                  ['V', 'G', 'V', 'G', 'G']]
         },
  'players':[
             {'name':'playerA', 'primary-weapon':{'name':'laser', 'level':1},
               'secondary-weapon':{'name':'mortar', 'level':1}, 'health':100,
               'score':0, 'position':'4,0'},
         {'name':'you', 'primary-weapon':{'name':'mortar', 'level':1},
              'secondary-weapon':{'name':'droid', 'level':1}, 'health':100,
          'score':0, 'position':'0,4'},
        ]
 }";
            GameState gameState = JsonConvert.DeserializeObject<GameState>(fromServer);
            this.handler.Handle(gameState);
            this.pwner.Received(1).Pwn(gameState);
        }


        [Test]
        public void Connected_OnHandShakeSuccess()
        {
            const string fromServer = "{\"message\":\"connect\", \"status\":true}";
            GameState gameState = JsonConvert.DeserializeObject<GameState>(fromServer);
            this.handler.Handle(gameState);
            this.dispatcher.Received(1).OnHandShakeSuccess();
        }


        [Test]
        public void EndTurn_Endturn()
        {
            const string fromServer = "{'message':'endturn'}";
            GameState gameState = JsonConvert.DeserializeObject<GameState>(fromServer);
            this.handler.Handle(gameState);
            this.dispatcher.Received(1).OnEndturn();
        }


        [Test]
        public void ServerActionBroadcastUpgrade_PrintAction()
        {
            const string fromServer = "{'message':'action', 'type':'upgrade', 'weapon':'mortar','from':'username'}";
            GameState gameState = JsonConvert.DeserializeObject<GameState>(fromServer);
            this.handler.Handle(gameState);
            this.dispatcher.Received(1).PrintAction(gameState);
        }


        [Test]
        public void ServerMoveBroadcast_PrintAction()
        {
            const string fromServer = "{'message':'action', 'type':'move','direction':'up','from':'username' }";
            GameState gameState = JsonConvert.DeserializeObject<GameState>(fromServer);
            this.handler.Handle(gameState);
            this.dispatcher.Received(1).PrintAction(gameState);
        }
    }
}