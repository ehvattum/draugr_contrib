﻿#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace draugr_contrib
{
    [Serializable]
    public class GameState
    {
        [NonSerialized]
        private List<Resource> resources;

        public string Error { get; set; }
        public Map Map { get; set; }
        public string Message { get; set; }
        public List<Player> Players { get; set; }

        public List<Resource> Resources

        {
            get { return this.resources; }
            set { this.resources = value; }
        }

        public bool Status { get; set; }
        public int Turn { get; set; }
    }

    public class Resource
    {
        public string Type { get; set; }
    }

    [Serializable]
    public class Map
    {
        public TileType[][] Data { get; set; }

        [JsonProperty("j-length")]
        public int Jlength { get; set; }

        [JsonProperty("k-length")]
        public int Klength { get; set; }


        public int GetReourceCount(TileType tileType)
        {
            return Data.SelectMany(jcollumn => jcollumn).Count(tile => tile == tileType);
        }
    }

    [Serializable]
    public class Player
    {
        public string Health { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }

        [JsonProperty("primary-weapon")]
        public Weapon PrimaryWeapon { get; set; }

        public int Score { get; set; }

        [JsonProperty("secondary-weapon")]
        public Weapon SecondaryWeapon { get; set; }


        public string[] Coords()
        {
            return Position.Split(',');
        }
    }

    [Serializable]
    public class Weapon
    {
        public int Level { get; set; }
        public string Name { get; set; }


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((Weapon)obj);
        }


        public override int GetHashCode()
        {
            unchecked
            {
                return (Level * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }


        protected bool Equals(Weapon other)
        {
            return Level == other.Level && string.Equals(Name, other.Name);
        }
    }
}