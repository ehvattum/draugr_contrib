#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;
using System.IO;
using System.Text;

namespace draugr_contrib
{
    public class StreamHandler : IStreamHandler
    {
        private readonly ASCIIEncoding encoding;
        private readonly StreamReader reader;
        private readonly StreamWriter writer;


        public StreamHandler(Stream stream)
        {
            this.encoding = new ASCIIEncoding();
            this.writer = new StreamWriter(stream, this.encoding) { NewLine = "\n", AutoFlush = true };
            this.reader = new StreamReader(stream, this.encoding);
        }


        public string GetResponse()
        {
            string response = this.reader.ReadLine();
            Console.WriteLine("<<<: " + response);
            return response;
        }


        public void SendCommand(string command)
        {
            Console.WriteLine(">>>: " + command);
            this.writer.WriteLine(command);
        }
    }
}