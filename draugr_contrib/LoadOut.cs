#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;

using Newtonsoft.Json;

namespace draugr_contrib
{
    [Serializable]
    public class Loadout
    {
        public Loadout(string primary, string secondary)
        {
            Message = "loadout";
            PrimaryWeapon = primary;
            SecondaryWeapon = secondary;
        }


        [JsonProperty("message")]
        public string Message { get; private set; }

        [JsonProperty("primary-weapon")]
        public string PrimaryWeapon { get; private set; }

        [JsonProperty("secondary-weapon")]
        public string SecondaryWeapon { get; private set; }
    }
}