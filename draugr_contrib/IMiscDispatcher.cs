#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;

namespace draugr_contrib
{
    public interface IMiscDispatcher
    {
        void OnEndturn();
        void OnHandShakeSuccess();
        void PrintAction(GameState gameState);
    }

    public class MiscDispatcher : IMiscDispatcher
    {
        public void OnEndturn()
        {
            Console.WriteLine("Turn over!");
        }


        public void OnHandShakeSuccess()
        {
            Console.WriteLine("Handshake successful");
        }


        public void PrintAction(GameState state)
        {
            Console.WriteLine("Someone did something");
        }
    }
}