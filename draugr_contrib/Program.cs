﻿#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;
using System.Net;
using System.Net.Sockets;

using Newtonsoft.Json;

namespace draugr_contrib
{
    public class Program
    {
        private static void Main()
        {
            Actions possibleActions = new Actions();
            StreamHandler streamHandler;
            IResponseHandler responseHandler;

            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 54321);
                client.Connect(serverEndPoint);
                NetworkStream clientStream = client.GetStream();
                streamHandler = new StreamHandler(clientStream);
                IPwner pwner = new Pwner(streamHandler, possibleActions);
                IMiscDispatcher miscDispatcher = new MiscDispatcher();
                responseHandler = new ResponseHandler(pwner,miscDispatcher);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error establishing connection: " + e.Message);
                return;
            }

            while (true)
            {
                string response = streamHandler.GetResponse();
                if (response == null)
                    break;
                GameState state;
                try
                {
                    state = JsonConvert.DeserializeObject<GameState>(response);
                    Console.WriteLine("--GOT STATE--");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed deserializing the response '" + response + "':" + e.Message);
                    return;
                }
                responseHandler.Handle(state);
            }
        }
    }
}