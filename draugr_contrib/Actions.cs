﻿#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using Newtonsoft.Json;

namespace draugr_contrib
{
    public class Actions
    {
        public string EndTurn()
        {
            return JsonConvert.SerializeObject(new { message = "endturn" });
        }


        public string HandShake()
        {
            return JsonConvert.SerializeObject(new { message = "connect", name = "draugr", revision = 1 });
        }


        public string Loadout(string primary, string secondary)
        {
            return JsonConvert.SerializeObject(new Loadout(primary, secondary));
        }


        public string Mine()
        {
            return JsonConvert.SerializeObject(new { message = "action", type = "mine" });
        }


        public string Move(Directions direction)
        {
            return JsonConvert.SerializeObject(new { message = "action", type = "move", direction });
        }


        public string ShootDroid(Directions[] sequence)
        {
            return JsonConvert.SerializeObject(new { message = "action", type = "droid", sequence });
        }


        public string ShootLaser(Directions direction)
        {
            return JsonConvert.SerializeObject(new { message = "action", type = "laser", direction });
        }


        /// <summary>
        ///   Shoots the mortar, relative to players coordinates
        /// </summary>
        /// <param name="coordinates"> coordinates relative to player </param>
        /// <returns> Json doing the proper action </returns>
        public string ShootMortar(string coordinates)
        {
            return JsonConvert.SerializeObject(new { message = "action", type = "mortar", coordinates });
        }


        public string Upgrade(Weapon weapon)
        {
            return JsonConvert.SerializeObject(new { message = "action", type = "upgrade", weapon = weapon.Name });
        }
    }
}