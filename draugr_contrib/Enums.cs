#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;
using System.Runtime.Serialization;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace draugr_contrib
{
    [Serializable]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Directions
    {
        up,

        down,

        [EnumMember(Value = "right-up")]
        rightUp,

        [EnumMember(Value = "right-down")]
        rightDown,

        [EnumMember(Value = "left-up")]
        leftUp,

        [EnumMember(Value = "left-down")]
        leftDown
    }

    [Serializable]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TileType
    {
        [EnumMember(Value = "G")]
        GRASS,

        [EnumMember(Value = "V")]
        VOID,

        [EnumMember(Value = "S")]
        SPAWN,

        [EnumMember(Value = "E")]
        EXPLOSIUM,

        [EnumMember(Value = "R")]
        RUBIDIUM,

        [EnumMember(Value = "C")]
        SCRAP,

        [EnumMember(Value = "O")]
        ROCK
    }
}