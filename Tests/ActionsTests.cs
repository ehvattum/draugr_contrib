﻿#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using NUnit.Framework;

using draugr_contrib;

namespace Tests
{
    [TestFixture]
    public class ActionsTests
    {
        #region Setup/Teardown

        [SetUp]
        public void Setup()
        {
            this.actions = new Actions();
        }

        #endregion

        private Actions actions;


        [Test]
        public void EndTurnTest()
        {
            const string expected = "{\"message\":\"endturn\"}";
            Assert.That(this.actions.EndTurn(), Is.EqualTo(expected));
        }


        [Test]
        public void HandShakeTest()
        {
            const string expected = "{\"message\":\"connect\",\"name\":\"draugr\",\"revision\":1}";
            Assert.That(this.actions.HandShake(), Is.EqualTo(expected));
        }


        [Test]
        public void LoadOutTest()
        {
            const string expected =
                "{\"message\":\"loadout\",\"primary-weapon\":\"laser\",\"secondary-weapon\":\"mortar\"}";
            Assert.That(this.actions.Loadout("laser", "mortar"), Is.EqualTo(expected));
        }


        [Test]
        public void MineTest()
        {
            const string expected = "{\"message\":\"action\",\"type\":\"mine\"}";
            Assert.That(this.actions.Mine(), Is.EqualTo(expected));
        }


        [Test]
        public void MoveTest()
        {
            const string up = "{\"message\":\"action\",\"type\":\"move\",\"direction\":\"up\"}";
            Assert.That(this.actions.Move(Directions.up), Is.EqualTo(up));

            const string down = "{\"message\":\"action\",\"type\":\"move\",\"direction\":\"down\"}";
            Assert.That(this.actions.Move(Directions.down), Is.EqualTo(down));

            const string leftdown = "{\"message\":\"action\",\"type\":\"move\",\"direction\":\"left-down\"}";
            Assert.That(this.actions.Move(Directions.leftDown), Is.EqualTo(leftdown));

            const string leftup = "{\"message\":\"action\",\"type\":\"move\",\"direction\":\"left-up\"}";
            Assert.That(this.actions.Move(Directions.leftUp), Is.EqualTo(leftup));

            const string rightup = "{\"message\":\"action\",\"type\":\"move\",\"direction\":\"right-up\"}";
            Assert.That(this.actions.Move(Directions.rightUp), Is.EqualTo(rightup));

            const string rightdown = "{\"message\":\"action\",\"type\":\"move\",\"direction\":\"right-down\"}";
            Assert.That(this.actions.Move(Directions.rightDown), Is.EqualTo(rightdown));
        }


        [Test]
        public void ShootDroidTest()
        {
            const string expected =
                "{\"message\":\"action\",\"type\":\"droid\",\"sequence\":[\"up\",\"right-up\",\"right-down\",\"down\"]}";
            Assert.That(
                this.actions.ShootDroid(new[]
                                        { Directions.up, Directions.rightUp, Directions.rightDown, Directions.down, }),
                Is.EqualTo(expected));
        }


        [Test]
        public void ShootLaserTest()
        {
            const string up = "{\"message\":\"action\",\"type\":\"laser\",\"direction\":\"up\"}";
            Assert.That(this.actions.ShootLaser(Directions.up), Is.EqualTo(up));

            const string down = "{\"message\":\"action\",\"type\":\"laser\",\"direction\":\"down\"}";
            Assert.That(this.actions.ShootLaser(Directions.down), Is.EqualTo(down));

            const string leftdown = "{\"message\":\"action\",\"type\":\"laser\",\"direction\":\"left-down\"}";
            Assert.That(this.actions.ShootLaser(Directions.leftDown), Is.EqualTo(leftdown));

            const string leftup = "{\"message\":\"action\",\"type\":\"laser\",\"direction\":\"left-up\"}";
            Assert.That(this.actions.ShootLaser(Directions.leftUp), Is.EqualTo(leftup));

            const string rightup = "{\"message\":\"action\",\"type\":\"laser\",\"direction\":\"right-up\"}";
            Assert.That(this.actions.ShootLaser(Directions.rightUp), Is.EqualTo(rightup));

            const string rightdown = "{\"message\":\"action\",\"type\":\"laser\",\"direction\":\"right-down\"}";
            Assert.That(this.actions.ShootLaser(Directions.rightDown), Is.EqualTo(rightdown));
        }


        [Test]
        public void ShootMortarTest()
        {
            const string expected = "{\"message\":\"action\",\"type\":\"mortar\",\"coordinates\":\"3,2\"}";
            Assert.That(this.actions.ShootMortar("3,2"), Is.EqualTo(expected));
        }


        [Test]
        public void Upgrade()
        {
            const string expected = "{\"message\":\"action\",\"type\":\"upgrade\",\"weapon\":\"laser\"}";
            Weapon weapon = new Weapon { Level = 1, Name = "laser" };
            Assert.That(this.actions.Upgrade(weapon), Is.EqualTo(expected));
        }
    }
}