#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

namespace draugr_contrib
{
    public interface IStreamHandler
    {
        void SendCommand(string command);
    }
}