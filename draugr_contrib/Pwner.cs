#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using System;

namespace draugr_contrib
{
    public class Pwner : IPwner
    {
        private readonly Actions actions;
        private readonly IStreamHandler streamHandler;
        private GameState gameState;


        public Pwner(IStreamHandler streamHandler, Actions actions)
        {
            this.streamHandler = streamHandler;
            this.actions = actions;
        }


        public void Pwn(GameState state)
        {
            if (state == null)
                throw new ArgumentNullException("state");
            this.gameState = state;
        }
    }
}