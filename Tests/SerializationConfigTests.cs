﻿#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

using NUnit.Framework;

using Newtonsoft.Json;

using draugr_contrib;

namespace Tests
{
    [TestFixture]
    public class SerializationConfigTests
    {
        [Test]
        public void DeserializationInitGameState()
        {
            const string fromServer = @"{'message':'gamestate',
  'turn': 0,
  'map': {'j-length': 5,
          'k-length': 5,
          'data': [
                  ['R', 'G', 'V', 'G', 'V'],
                  ['G', 'E', 'G', 'G', 'S'],
              ['G', 'G', 'O', 'G', 'G'],
              ['S', 'G', 'G', 'C', 'G'],
                  ['V', 'G', 'V', 'G', 'G']]
         },
  'players':[
             {'name':'playerA', 'primary-weapon':{'name':'laser', 'level':1},
               'secondary-weapon':{'name':'mortar', 'level':1}, 'health':100,
               'score':0, 'position':'4,0'},
         {'name':'you', 'primary-weapon':{'name':'mortar', 'level':1},
              'secondary-weapon':{'name':'droid', 'level':1}, 'health':100,
          'score':0, 'position':'0,4'},
        ]
 }";
            GameState state = JsonConvert.DeserializeObject<GameState>(fromServer);
            Assert.That(state.Players.Count, Is.EqualTo(2),"players");
            Assert.That(state.Turn, Is.EqualTo(0),"turn");

            Assert.That(state.Map.Jlength, Is.EqualTo(5),"Jlength");
            Assert.That(state.Map.Klength, Is.EqualTo(5),"Klenght");

            CollectionAssert.AreEqual(state.Players[0].Coords(),new[]{"4","0"});
            CollectionAssert.AreEqual(state.Players[1].Coords(),new[]{"0","4"});

            Assert.That(state.Players[0].PrimaryWeapon,Is.EqualTo(new Weapon{Level = 1,Name = "laser"}));
            Assert.That(state.Players[0].SecondaryWeapon,Is.EqualTo(new Weapon{Level = 1,Name = "mortar"}));
            Assert.That(state.Players[1].PrimaryWeapon,Is.EqualTo(new Weapon{Level = 1,Name = "mortar"}));
            Assert.That(state.Players[1].SecondaryWeapon,Is.EqualTo(new Weapon{Level = 1,Name = "droid"}));

            Assert.That(state.Map.GetReourceCount(TileType.RUBIDIUM), Is.EqualTo(1),TileType.RUBIDIUM.ToString());
            Assert.That(state.Map.GetReourceCount(TileType.ROCK), Is.EqualTo(1), TileType.ROCK.ToString());
            Assert.That(state.Map.GetReourceCount(TileType.EXPLOSIUM), Is.EqualTo(1), TileType.EXPLOSIUM.ToString());
            Assert.That(state.Map.GetReourceCount(TileType.SCRAP), Is.EqualTo(1), TileType.SCRAP.ToString());
            Assert.That(state.Map.GetReourceCount(TileType.VOID), Is.EqualTo(4), TileType.VOID.ToString());
            Assert.That(state.Map.GetReourceCount(TileType.GRASS), Is.EqualTo(15), TileType.GRASS.ToString());
            Assert.That(state.Map.GetReourceCount(TileType.SPAWN), Is.EqualTo(2), TileType.SPAWN.ToString());
        }
    }
}