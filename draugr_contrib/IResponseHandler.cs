#region creds

// --------------------------------------------------
// ragesheep aka baconbike production
// --------------------------------------------------

#endregion

namespace draugr_contrib
{
    public interface IResponseHandler
    {
        void Handle(GameState gameState);
    }
}